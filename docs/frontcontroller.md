# Front Controller

Change into dev folder
```
cd kiowa
```

Now we will create the HomepageController.php file and the view is included.

```
php bin/console make:controller
```
call it **HomepageController** if are following this tutorial.

It should look like This
```
/opt/lampp/htdocs/kiowa$ php bin/console make:controller

 Choose a name for your controller class (e.g. GrumpyPuppyController):
 > HomepageController
```

Edit the route in the HomepageController file.

From this:
```
/**
 * @Route("/homepage", name="homepage")
 */
public function index()
```

to this:
```
/**
 * @Route("/", name="homepage")
 */
public function index()
```

### If all went well
you should see something like this:
<img src="./img/HomepageController_2018-10-12_00-37-57.png"></img>

-------------

## Get some images

For this torial we will need some images.

NOTE!!! I am NOT affiliated in any way with any linked sites I may use. I personally enjoy those sites and really find them to be great. - By the way [PixaBay](http://pixaby.com) is a great source for all kind of images. FREE!!!

Now download some flag icons from [famfam](http://www.famfamfam.com/lab/icons/flags/)

unzip the *.png* files to `public/images/`

rename `public/images/png/` to `public/images/flags/`


----

That's it for this part.
