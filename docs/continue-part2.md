# Setup site

## Get your datadase set

Open the *.env* file and find this:
```
DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/db_name
```
Replace
* *db_user* with *YOUR DATABASE USER NAME*
* *db_password* with *YOUR DATABASE PASSWORD*
* *db_name* with *YOUR DATABASE NAME*

...and save.

##### If your database is not created yet do that now. On the command line, you can do: `php bin/console doctrine:database:create`

## Get a versioning system Setup
I want to use an (semi-)automated way of versioning this software. I'm big fan of using []().

For this reason, I have choosen Shiva's Versioning Bundle to help us out with that.
```
composer require shivas/versioning-bundle
```
Now test the versioning system
```
# Display a dry run of a version bump
bin/console app:version:bump -d
```
