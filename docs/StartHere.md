# Steps to follow

## Requirements
* Symfony installer on your system
* Composer installed (system wide - suggested)

*<font color=""><small>I will do a tutorial on that based off of my old tut on the subject, if needed.</small></font>*

### Start project with composer

Change "kiowa" to your project name (if you want)
```
composer create-project symfony/website-skeleton kiowa
```

### Move to dir

Change "kiowa" to your project name (if needed)
```
cd kiowa
```

### Get Symfony's server package

```
composer require server --dev
```

**Now, get the server running**

```
./bin/console server:start
```

### Create an .htaccess file

Mein is like this. Test it out and see if it works for you.

Now COPY & PASTE the following:
```
# Enable rewrite engine and route requests to framework
RewriteEngine On
RewriteBase /

RewriteCond %{REQUEST_URI} ^/robots.txt
RewriteRule ^(.*)$ /public/robots.txt [L]

# resources
RewriteCond %{REQUEST_URI} ^/images/ [OR]
RewriteCond %{REQUEST_URI} ^/js/ [OR]
RewriteCond %{REQUEST_URI} ^/fonts/ [OR]
RewriteCond %{REQUEST_URI} ^/css/
RewriteRule ^(.*)$ /public/$1 [L]

RewriteCond %{REQUEST_URI} !^/public/
RewriteRule ^(.*)$ /public/$1 [QSA,L]

Browse to http://127.0.0.1:8000, you should get an error message and debug bar at the bottom.
```

#### Your site in dev mode

Launch youre browser and paste this
```
http://127.0.0.1:8000
```
...and you can now see your site grow or even help you with debugging

##### NOTE: This is a good time to push to git (or svn, if you are still using that)


======
