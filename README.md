# Kiowa

is a Multilingual Content Management System. Built as a full scale blog, but with some sort of expansion system, we will have a CMS as is needed. We start off with utilizing the <a target="_blank" rel="noopener noreferrer" href="https://symfony.com">Symfony4 Framework</a> and <a target="_blank" rel="noopener noreferrer" href="https://getcomposer.com">Composer</a>.

## Features (planned features)
These are things that I normally need on any system I use for building a website.

  *  Multilanguage
  *  Translation method for the content
  *  Cookie information
  *  Legal information stuf
  *  Blog or News pages
  *  Static Pages
  *  Captcha
  *  Themes or Skins
  *  Modules, extensions, plugins
  *  Admin Area
  *  User stuff
  *  Method to install update, theme, modules in the backend


## Documentation
Will have development docs as I develop in the ~~[docs folder](docs).~~

~~These may get added to the~~ [WIKI](wiki) ~~at some point~~.

Other docs as soon as is usable.


## License
<i aria-hidden="true" data-hidden="true" class="fa fa-balance-scale fa-fw"></i>
This project is licensed under the
[**MIT License**](LICENSE).

<a target="_blank" rel="noopener noreferrer" href="http://choosealicense.com/licenses/mit/">Learn more</a>

-----
