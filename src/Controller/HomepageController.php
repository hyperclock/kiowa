<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomepageController extends AbstractController
{
  /**
   * @Route("/", name="homepage")
   */
  public function index()
  {
      return $this->render('homepage/index.html.twig');
  }

  /**
   * @Route("/about", name="about")
   */
  public function about() {
      return $this->render('about/index.html.twig');
  }

  /**
   * @Route("/services", name="services")
   */
  public function services() {
      return $this->render('services/index.html.twig');
  }
  
  /**
   * @Route("/contact", name="contact")
   */
  public function contact() {
      return $this->render('contact/index.html.twig');
  }

  /**
   * @Route("/search", name="search")
   */
  public function search() {
      return $this->render('search/index.html.twig');
  }

  /**
   * @Route("/sidebar", name="sidebar")
   */
  public function sidebar() {
      return $this->render('sidebar/index.html.twig');
  }
}
